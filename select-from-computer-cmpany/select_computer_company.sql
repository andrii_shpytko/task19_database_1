show schemas;
use computer_company;
show tables;

describe laptop;
describe pc;
describe printer;
describe product;

select * from pc;

#1
select maker, type 
	from product 
		where type = 'laptop'
			order by maker;

#2
select model, ram, screen, price
	from laptop
		where price > 1000
			order by ram asc, price desc;
            
#3
select *
	from printer
		where color = 'y'
			order by price desc;
            
select * from printer;
#4
select model, speed, hd, cd, price
	from pc
		where cd = '12x' or cd = '24x' and price < 600
			order by cd asc;
#4
select model, speed, hd, cd, price
	from pc
		where (cd = '12x' or cd = '24x') and (price < 600)
			order by cd asc;





            
show schemas;
create schema if not exists computer_company;
-- drop schema if exists computer_company;
use computer_company;
show tables;

drop table if exists laptop;
drop table if exists pc;
drop table if exists printer;
drop table if exists product;

create table if not exists laptop(
	code int not null,
    model varchar (50) not null,
    speed smallint not null,
    ram smallint not null,
    hd real not null,
    price decimal (8, 2) null,
    screen tinyint not null
);

create table if not exists pc(
	code int not null,
    model varchar (50) not null,
    speed smallint not null,
    ram smallint not null,
    hd real not null,
    cd varchar (10) not null,
    price decimal (8, 2) null
);

create table if not exists printer(
	code int not null,
    model varchar (50) not null,
    color char (1) not null,
    type varchar (10) not null,
    price decimal (8, 2) null
);

create table if not exists product(
	maker varchar (10) not null,
    model varchar (50) not null,
    type varchar (50) not null
);


alter table laptop add constraint pk_laptop primary key NONCLUSTERED(code);
alter table pc add constraint pk_pc primary key NONCLUSTERED (code);
alter table printer add constraint pk_printer primary key NONCLUSTERED(code);
alter table product add constraint pk_product primary key NONCLUSTERED(model);

alter table printer add constraint fk_printer_product foreign key (model) references product (model);
alter table pc add constraint fk_pc_product foreign key (model) references product (model);
alter table laptop add constraint fk_laptop_product foreign key (model) references product (model);



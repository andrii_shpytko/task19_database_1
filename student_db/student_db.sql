create schema if not exists task19_database_1_studends_db;
-- drop schema task19_database_1_studends_db;
use task19_database_1_studends_db;
show tables;

create table region(
    id int primary key auto_increment,
    name int not null,
    code char(3) not null,
    unique key (name),
    unique key (code)
);

create table city(
    id int primary key auto_increment,
    name varchar(20) not null,
    region_id int not null,
    foreign key (region_id) references region (id) on update cascade on delete restrict
);

create table school(
    id int primary key auto_increment,
    name varchar(30) not null,
    phone char(10),
    headmaster varchar(30) not null,
	street varchar(50) not null,
    numb_building varchar(5) not null,
    city_id int not null,
    unique key (phone),
    foreign key (city_id) references city (id) on update cascade on delete restrict
);

create table student_group(
    id int primary key auto_increment,
    name varchar(20) not null,
    number int not null,
    admission_year year not null,
    unique key (name),
    unique key (number)
);

create table student(
    id int primary key auto_increment,
    first_name varchar(20) not null,
    second_name varchar(20) not null,
    last_name varchar(20) not null,
    birth_date date not null,
    rating smallint not null,
    city_id int not null,
    school_id int null,
    student_group_id int null,
    admission_date date not null,
    student_card_number char(10) not null,
    email varchar(50),
    unique key (student_card_number),
    unique key (email),
    foreign key (city_id) references city (id) on update cascade on delete restrict,
    foreign key (school_id) references school (id) on update cascade on delete set null,
    foreign key (student_group_id) references student_group (id) on update cascade on delete set null
);

create table debt(
    id int primary key auto_increment,
    subject varchar(30) not null,
    unique key (subject)
);

create table student_debt(
    id int primary key auto_increment,
    student_id int not null,
    debt_id int not null,
    foreign key (student_id) references student(id),
    foreign key (debt_id) references debt(id)
)

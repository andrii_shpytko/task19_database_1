show schemas;
use Labor_SQL;
show tables;

describe laptop;
describe pc;
describe printer;
describe product;

#1
select *
	from pc
		where model like '%1%1%';

#2
select *
	from outcome as o
		where month(date) = 03;

#3
select *
	from outcome_o as o
		where day(date) = 14;

#4
select s.name
	from ships as s
		where s.name rlike '^w[:alnum:]+n$';

#5
select s.name
	from ships as s
		where s.name like '%e%e%';

#6
select s.name, s.launched
	from ships as s
		where s.name rlike '[^a]$';

#7
select b.name
	from battles as b
		where b.name like '% %' and b.name rlike '[^c]$';

#8
select *
	from trip as t
		where hour(t.time_out) between 12 and 17 
			or (hour(t.time_out) = 17 and minute(t.time_out) = 00);
#8a
select *
    from trip as t
		where not hour(t.time_out) between 5 and 23;
#8b
select *
	from trip as t
		where hour(t.time_out) between 1 and 3;
#8b
select *
	from trip as t
		where hour(t.time_in) between 1 and 3;
select *
	from trip as t
		where hour(t.time_in) between 1 and 3
			or (hour(t.time_in) = 3 and minute(t.time_in) = 15);

#9
select *
	from trip as t
		where hour(t.time_out) between 17 and 23
			or (hour(t.time_in) = 23 and minute(t.time_in) = 00);

#10
select *
	from pass_in_trip as p
		where place like '1%'
	order by date;

#11
select *
	from pass_in_trip as p
		where p.place like '%c'
	order by date asc;
select p.date
	from pass_in_trip as p
		where p.place like '%c'
	order by date desc;

#12
select substring_index(name, ' ', -1)
	from passenger as p
		where p.name like '% c%';
select p.name
	from passenger as p
		order by p.name desc;

#13
select substring_index(name, ' ', -1)
	from passenger as p
		where p.name like '% j%';




show schemas;
use Labor_SQL;
show tables;

describe laptop;
describe pc;
describe printer;
describe product;

select * from pc;

#1
select maker, type 
	from product 
		where type = 'laptop'
	order by maker;

#2
select model, ram, screen, price
	from laptop
		where price > 1000
	order by ram asc, price desc;
            
#3
select *
	from printer as p
		where p.color = 'y'
	order by p.price desc;
            
select * from printer;
#4
select model, speed, hd, cd, price
	from pc
		where cd = '12x' or cd = '24x' and price < 600
	order by cd asc;
#4
select model, speed, hd, cd, price
	from pc
		where (cd = '12x' or cd = '24x') and (price < 600)
	order by cd asc;

#5
select s.name, s.class
	from ships as s
		order by s.name asc;

#6
select * from pc
	where speed >= 500 and price <= 800
		order by price desc;
select * from pc where speed >= 500 and price <= 800 order by price desc;

#7
select * from printer
	where type != 'matrix' and price <= 300
		order by type desc;

#8
select pc.model, pc.speed
	from pc
		where pc.price between 400 and 600
			order by hd asc;
#8a
select pc.model, pc.speed
	from pc
		where pc.price >= 400 and pc.price <= 600
			order by hd asc;

#9
select pc.model, pc.speed, pc.hd 
	from pc
		join product as p on pc.model = p.model
    where pc.hd = 10 or pc.hd = 20 and maker = 'A'
		order by pc.speed asc;

#10
select l.model, l.speed, l.hd, l.price
	from laptop as l
		where l.screen >= 12
    order by l.price desc;

#11
select p.model, p.type, p.price
	from printer as p
		where p.price < 300
    order by p.type desc;

#12
select l.model, l.ram, l.price
	from laptop as l
		where l.ram = 64
	order by l.screen asc;

#13
select pc.model, pc.ram, pc.price
	from pc
		where ram > 64
	order by pc.hd asc;

#14
select pc.model, pc.speed, pc.price
	from pc
		where pc.speed between 500 and 750
	order by pc.hd desc;
#14a
select pc.model, pc.speed, pc.price
	from pc
		where pc.speed >=500 and pc.speed <= 750
	order by pc.hd desc;
#14b
select pc.model, pc.speed, pc.price
	from pc
		where pc.speed >= 500 and pc.speed <= 750
	order by pc.hd desc, pc.price desc;

#15
select *
	from outcome_o as o
		where o.out > 2000
	order by o.date desc;

#16
select *
	from income_o as i
		where i.inc between 5000 and 10000
	order by i.inc asc;
#16a
select *
	from income_o as i
		where i.inc >= 5000 and i.inc <= 10000
	order by i.inc asc;

#17
select *
	from income as i
		where i.point = 1
	order by i.inc asc;

#18
select *
	from outcome as o
		where o.point = 2
	order by o.out asc;

#19
select *
	from ships as s
		join classes as c on s.class = c.class
			where c.country = 'japan'
		order by c.type desc; 

#20
select s.name, s.launched
	from Ships as s
		join classes as c on s.class = c.class
			where s.launched between 1920 and 1942
		order by s.launched desc;
select s.name, s.launched
	from Ships as s
		where s.launched between 1920 and 1942
	order by s.launched desc;
select s.name, s.launched
	from Ships as s
		where s.launched >= 1920 and s.launched <= 1942
	order by s.launched desc;

#21
select o.ship, o.battle, o.result 
	from outcomes as o
		where o.battle = 'Guadalcanal' and (o.result = 'OK' or o.result = 'damaged')
	order by ship desc;

#22
select o.ship, o.battle, o.result
	from outcomes as o
		where o.result = 'sunk'
	order by o.ship desc;

#23
select c.class, c.displacement
	from classes as c
		where c.displacement >= 40
	order by c.type;

#24
select t.trip_no, t.town_from, t.town_to
	from trip as t
		where t.town_from = 'london' or t.town_to = 'london'
	order by t.time_out;

#25
select t.trip_no, t.plane, t.town_from, t.town_to
	from trip as t
		where t.plane = 'tu-134'
	order by t.time_out;

#26
select t.trip_no, t.plane, t.town_from, t.town_to
	from trip as t
		where t.plane != 'il-86'
	order by t.plane asc;

#27
select t.trip_no, t.town_from, t.town_to
	from trip as t
		where t.town_from != 'rostov' and t.town_to != 'rostov'
	order by t.plane asc;

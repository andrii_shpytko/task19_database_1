show schemas;
use Labor_SQL;
show tables;

#1
select pc.model, p.type, pc.speed, pc.hd
	from pc
		join product as p on pc.model = p.model
			where pc.hd <= 8;
select l.model, p.type, l.speed, l.hd, l.price 
	from laptop as l 
		join product as p on l.model = p.model 
			where l.hd >= 8 
		order by l.price desc;

#2
select p.maker
	from product as p
		join pc on pc.model = p.model
			where pc.speed >= 600;
select p.maker, l.price 
	from laptop as l 
		join product as p on l.model = p.model 
			where l.speed between 600 and 800
		order by l.price desc;

#3
select p.maker
	from product as p
		join laptop as l on p.model = l.model
			where l.speed <= 500;

#4
select l1.model as 'L1 model', l1.ram as 'L1 ram', l1.hd as 'hd', l2.model as 'L2 model', l2.ram as 'L2 ram' 
	from laptop as l1, laptop as l2 
        where l1.hd = l2.hd and l1.code > l2.code 
	order by l1.code desc;
#4a
select l1.model as 'L1 model', l1.ram as 'L1 ram', l1.hd as 'hd', l2.model as 'L2 model', l2.ram as 'L2 ram'
	from laptop as l1 
		join laptop as l2 on l1.hd = l2.hd 
			where l1.code > l2.code 
		order by l1.code desc;
#4b
select l1.code as 'L1 code', L1.model as 'L1 model', l2.code as 'L2 code', L2.model as 'L2 model', L1.hd as 'hd', 
L1.ram as 'L1 ram', L2.ram as 'L2 ram', l1.price as 'L1 price', l2.price as 'L2 price' 
	from Laptop as L1, Laptop as L2 
		where L1.hd = L2.hd and L1.code > L2.code 
	order by l1.code desc;
#4c

#5
select c1.country, c1.class, c2.class 
	from classes as c1 
		join classes as c2 on c1.country = c2.country 
			where c1.type = 'bb' and c2.type = 'bc' ;

#6
select pc.model, p.maker
	from pc 
		join product as p on pc.model = p.model 
			where pc.price <= 600;
#6a
select pc.model, pc.speed, pc.ram, pc.hd, pc.cd, pc.price 
	from pc 
		join product as p on p.model = pc.model 
			where pc.price <= 600 
		order by price desc;

#7
select pr.model, p.maker 
	from printer pr 
		join product as p on p.model = pr.model 
			where pr.price > 300;

#8
select p.maker, pc.model, pc.price  
	from pc 
		join product as p on pc.model = p.model
	order by pc.price desc;
#8a
select p.maker, l.model, l.price  
	from laptop  as l 
		join product as p on p.model = l.model 
	order by l.price desc;
#8b
select p.type, p.maker, pc.model, pc.price 
	from pc 
		join product as p on pc.model = p.model 
	union 
    select p.type, p.maker, l.model, l.price 
		from laptop as l 
			join product as p on l.model = p.model 
	order by price;

#9
select p.type, p.maker, pc.model, pc.price 
	from pc 
		join product as p on pc.model = p.model 
	union 
    select p.type, p.maker, l.model, l.price 
		from laptop as l 
			join product as p on l.model = p.model 
	order by price desc;

#10
select p.maker, p.type, l.model, l.speed 
	from laptop as l 
		join product as p on l.model = p.model 
			where l.speed >= 600 
		order by l.speed;
#10a
select p.maker, p.type, l.model, l.speed 
	from laptop as l 
		join product as p on l.model = p.model 
			where l.speed >= 600 
	union 
    select p.maker, p.type, pc.model, pc.speed 
		from pc 
			join product as p on pc.model = p.model 
				where pc.speed >= 600 
	order by speed;

#11
select s.name, c.displacement 
	from classes as c 
		join ships as s on c.class = s.class 
	order by c.displacement desc, s.name asc;

#12
select o.ship, o.battle, b.date 
	from outcomes as o 
		join battles as b on o.battle = b.name 
			where o.result = 'ok';
#12a
select o.ship, o.battle, b.date, o.result 
	from outcomes as o 
		join battles as b on o.battle = b.name 
			where o.result = 'sunk' 
	union 
    select o.ship, o.battle, b.date, o.result 
		from outcomes as o 
			join battles as b on o.battle = b.name 
				where o.result = 'damaged' 
	order by b.date;

#13
select s.name as 'name ship', c.country as 'name country' 
	from classes as c 
		join ships as s on c.class = s.class ;

#14
select t.plane, c.name 
	from trip as t 
		join company as c on t.id_comp = c.id_comp 
			where t.plane = 'boeing' ;

#15
select p.name, p_t.date 
	from passenger as p 
		join pass_in_trip as p_t on p.id_psg = p_t.id_psg 
	order by p_t.date desc, p.name asc;
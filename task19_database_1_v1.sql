create schema if not exists task19_database_1_v1;
-- drop schema task19_database_1_v1;
use task19_database_1_v1;
show tables;

create table if not exists country(
	code char(3) primary key not null,
    name varchar(50) not null
) engine=InnoDB;

create table if not exists city(
	id int primary key auto_increment,
    name varchar(50) not null
) engine=InnoDB;

create table if not exists country_city(
	country_code char(3) not null,
    city_id int not null,
    primary key (country_code, city_id),
    foreign key (country_code) references country (code) on update cascade on delete restrict,
    foreign key (city_id) references city (id) on update cascade on delete restrict
) engine=InnoDB;

create table if not exists hotel_chain(
	id smallint primary key auto_increment,
    name varchar(50) not null
) engine=InnoDB;

create table if not exists hotel(
	id int primary key auto_increment,
    name varchar(50) not null,
    quantity_appartments tinyint,
    star enum('3', '4', '5')
) engine=InnoDB;

create table if not exists hotel_chain_hotel(
	hotel_chain_id smallint not null,
    hotel_id int not null,
    country_code char(3) not null,	###
    city_id int not null,	###
    primary key (hotel_chain_id, hotel_id),
    foreign key (hotel_chain_id) references hotel_chain (id) on update cascade on delete restrict,
    foreign key (hotel_id) references hotel (id) on update cascade on delete restrict,
    foreign key (country_code) references country_city (country_code) on update cascade on delete restrict,	###
    foreign key (city_id) references country_city (city_id) on update cascade on delete restrict	###
) engine=InnoDB;

create table if not exists appartment(
	id smallint primary key auto_increment ,
	quantity_rooms tinyint,
	quantity_seats tinyint,
	klass SET('luxury', 'standart', 'economy'),
    hotel_id int not null,
    foreign key (hotel_id) references hotel(id) on update cascade on delete restrict
) engine=InnoDB;

create table if not exists reservation(
	id int primary key auto_increment,
	start_date date,
    finish_date date,
    appartment_id smallint not null,
    foreign key (appartment_id) references appartment(id) on update cascade on delete restrict
) engine=InnoDB;

create table if not exists review(
	id int primary key auto_increment,
    review text,
    appartment_id smallint not null,
    foreign key (appartment_id) references appartment(id) on update cascade on delete restrict
) engine=InnoDB;
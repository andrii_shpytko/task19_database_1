show schemas;
create schema if not exists task19_database_1_family_tree_db;
drop schema if exists task19_database_1_family_tree_db;
use task19_database_1_family_tree_db;
show tables;

create table if not exists gender(
	id tinyint primary key auto_increment,
	type enum ('voman', 'man') primary key
);

create table if not exists family_satellite(
	first_name varchar (30) not null,
    last_name varchar (30) not null,
    gender_id tinyint not null,
    birth_date date,
    death_date date,
    birth_place varchar (40) not null,
    death_place varchar (40)
    
    
);
show schemas;
create database if not exists task19_database_1;
-- drop database if exists task19_database_1;
use task19_database_1;

show tables;

-- describe country;
-- describe city;
-- describe hotel_chain;
-- describe hotel;
-- describe appartment;
-- describe reservation;

drop table if exists country;
drop table if exists city;
drop table if exists hotel_network;
drop table if exists hotel;
drop table if exists appartment;
drop table if exists user;
drop table if exists reservation;
drop table if exists revies;

create table if not exists country(
	code char(3) primary key not null,
    name varchar(50) not null
) engine=InnoDB;

create table if not exists city(
	id int primary key auto_increment,
    name varchar(50) not null,
    country_code char(3) not null,
    foreign key (country_code) references country (code) on update cascade on delete restrict
) engine=InnoDB;

create table if not exists hotel_network(
	id smallint primary key auto_increment,
    name varchar(50) not null
) engine=InnoDB;

create table if not exists hotel(
	id int primary key auto_increment,
    name varchar(50) not null,
    quantity_appartments tinyint,
    star enum('3', '4', '5'),
    street varchar(50) not null,
    numb_building varchar(5) not null,
    hotel_network_id smallint not null,
    city_id int not null,
    foreign key (hotel_network_id) references hotel_network (id) on update cascade on delete restrict,
    foreign key (city_id) references city (id) on update cascade on delete restrict
) engine=InnoDB;

create table if not exists appartment(
	id smallint primary key auto_increment,
	quantity_rooms tinyint,
	quantity_seats tinyint,
	klass enum('luxury', 'standart', 'economy'),
    hotel_id int not null,
    foreign key (hotel_id) references hotel (id) on update cascade on delete restrict
) engine=InnoDB;

create table if not exists user(
	id int primary key auto_increment,
    name varchar(30) not null,
    surmane varchar(30) not null,
    gender enum('man', 'woman'),
    email varchar(50) not null,
    phone_number char(10),
    number_card char(16)
) engine=InnoDB;

create table if not exists reservation(
	id int primary key auto_increment,
	start_date date,
    finish_date date,
    appartment_id smallint not null,
    user_id int not null,
    foreign key (appartment_id) references appartment (id) on update cascade on delete restrict,
    foreign key (user_id) references user(id) on update cascade on delete restrict
) engine=InnoDB;

create table if not exists review(
	id int primary key auto_increment,
    appartment_id smallint not null,
    user_id int not null,
    foreign key (appartment_id) references appartment (id) on update cascade on delete restrict,
    foreign key (user_id) references user (id) on update cascade on delete restrict
) engine=InnoDB;
